@echo off
echo installing gulp and bower...

@call npm install -g gulp bower

echo installing node packages (npm install)...

@call npm install

echo installing gulp locally...

@call npm install gulp

echo installing bower packages (bower install)...

@call bower install

echo done!

echo run 'gulp serve' to load live server

echo run 'gulp' to build minified production ready version
@pause