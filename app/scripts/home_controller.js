app.controller('HomeController', function($scope, $log, $http, getItems) {
    $log.info('HomeController loaded');
    getItems.show($http, (response) => $scope.articles = response.data.content);
    $scope.Like = function (ev) {
        let img = ev.target.lastElementChild;
        //let img = ev.target; // if listener for image not for button
        let origin = location.origin;
        console.log(ev);
        if(img.src === `${origin}/assets/final-like-full.png`) {
            img.src = `${origin}/assets/final-like.png`;
        } else {
            img.src = `${origin}/assets/final-like-full.png`
        }
    };
});